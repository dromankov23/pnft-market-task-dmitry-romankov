import { postUser } from './fetch';
import './style.css';

const createInput = (
  type: string,
  name: string,
  labelString: string
): { input: HTMLInputElement; label: HTMLLabelElement } => {
  const label = document.createElement('label');
  label.setAttribute('for', name);
  label.innerText = labelString;
  const input = createElement('input', 'input') as HTMLInputElement;
  input.setAttribute('type', type);
  input.setAttribute('name', name);

  return {
    input,
    label,
  };
};

const createElement = (
  elem: string,
  className?: string,
  textContent?: string
): HTMLElement => {
  const element = document.createElement(elem);

  if (className) {
    element.setAttribute('class', className);
  }

  if (textContent) {
    element.innerText = textContent;
  }

  return element;
};

const formTitle = createElement('h2', 'form-title', 'NFT Access');
const formDescription = createElement(
  'p',
  'description',
  'Please fill your detail to access your account.'
);

const formHeading = createElement('div', 'form-heading');

formHeading.appendChild(formTitle);
formHeading.appendChild(formDescription);

const emailWrapper = createElement('div', 'input-wrapper');
const passwordWrapper = createElement('div', 'input-wrapper');

let inputEmailValue: string;
let inputPasswordValue: string;

const { input: emailInput, label: emailLabel } = createInput(
  'email',
  'email',
  'Email'
);

emailInput.addEventListener('change', (e: Event) => {
  const target = e.target as HTMLInputElement;
  inputEmailValue = target.value;
});

emailInput.setAttribute('required', 'true');

emailWrapper.appendChild(emailLabel);
emailWrapper.appendChild(emailInput);

const { input: passwordInput, label: passwordLabel } = createInput(
  'password',
  'password',
  'Password'
);

passwordInput.addEventListener('change', (e: Event) => {
  const target = e.target as HTMLInputElement;
  inputPasswordValue = target.value;
});

passwordInput.setAttribute('required', 'true');

passwordWrapper.appendChild(passwordLabel);
passwordWrapper.appendChild(passwordInput);

const { input: rememberCheckBox, label: checkBoxLabel } = createInput(
  'checkbox',
  'remember',
  'Remember me'
);

const forgotPasswordLink = createElement(
  'a',
  'forgot-link',
  'Forgot Password?'
);
forgotPasswordLink.setAttribute('href', '#');
const checkBoxContainer = createElement('div', 'checkbox-container');
checkBoxContainer.appendChild(rememberCheckBox);
checkBoxContainer.appendChild(checkBoxLabel);
checkBoxContainer.appendChild(forgotPasswordLink);

const signInButton = createElement('button', 'sign-in-button', 'Sign in');
const signInButtonGoogle = createElement(
  'button',
  'sign-in-button sign-in-google',
  'Sign in with Google'
);

const bottomLinkContainer = createElement('div', 'bottom-container');
const helperText = createElement('p', 'helper-text', "Don't have an account");
const signUpLink = createElement('a', 'sign-up', 'Sign up');
signUpLink.setAttribute('href', '#');

bottomLinkContainer.appendChild(helperText);
bottomLinkContainer.appendChild(signUpLink);

const form = createElement('form', 'form');
form.appendChild(emailWrapper);
form.appendChild(passwordWrapper);
form.appendChild(checkBoxContainer);
form.appendChild(signInButton);
form.appendChild(signInButtonGoogle);
form.appendChild(bottomLinkContainer);

form.addEventListener('submit', (e) => {
  e.preventDefault();
});

const formWrapper = createElement('div', 'form-wrapper');
formWrapper.appendChild(formHeading);
formWrapper.appendChild(form);

const image = createElement('img', 'image');
image.setAttribute('src', '../assets/DRIP_20.png');

const logoContainer = createElement('dev', 'logo-container');
const logo = createElement('img', 'logo');
const logoTitle = createElement('p', 'logo-title', 'PNFT Market');
logo.setAttribute('src', '../assets/logo.svg');

logoContainer.appendChild(logo);
logoContainer.appendChild(logoTitle);

const container = createElement('section', 'container');
container.appendChild(logoContainer);
container.appendChild(formWrapper);
container.appendChild(image);

document.querySelector<HTMLDivElement>('#app')?.appendChild(container);

const handlePostUser = async () => {
  const email = inputEmailValue;
  const password = inputPasswordValue;


  const result = await postUser(email, password);

  console.log(result);
};

signInButton.addEventListener('click', handlePostUser);
