interface User {
  username: string;
  password: string;
}

const fetchUsers = async (): Promise<User[] | unknown> => {
  try {
    const response = await fetch('./db/users.json');
    const users = await response.json();
    return users;
  } catch (err) {
    return err;
  }
};

export const postUser = async (email: string, password: string) => {
  const users = await fetchUsers() as User[];

  const isUserExist = users?.find(
    (user) => user.password === password && user.username === email
  );

  if (isUserExist) {
    return { code: 200, data: 'Success' };
  } else {
    return { code: 401, data: 'Invalid email or password' };
  }
};
